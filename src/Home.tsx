import React, {useState, useEffect} from 'react';
import { Button, View, Text, Image, ImageBackground, StyleSheet, Platform } from 'react-native';
import { NavigationContainer, TabActions } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { ScrollView, TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import { ChooseHabitButton, Circle, MainText, NewHabitInput, ShowHabits, SimpleButton, TabTitle } from '../Components/Components';
import { createStackNavigator } from '@react-navigation/stack';
import MaterialIcons from "react-native-vector-icons/MaterialIcons"
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"
import firestore from '@react-native-firebase/firestore';

const Stack = createStackNavigator()
const Tab = createBottomTabNavigator();
const HealthImage = require('./../assets/Images/4220878.jpg')
type RootStackParamList = {
    navigation:any
  };
function SplashScreen ({ navigation}:RootStackParamList){
    return(
        <View>
            <View style={{ justifyContent: 'center', alignItems:'center', paddingTop:150}}>
                <Image source={HealthImage} style={{width: 270, height: 270}}></Image>
                <MainText text="Practice and Track" active={true}/>
                <MainText text="Track your progress and keep going" active={false}></MainText>
            </View>
            <TouchableOpacity  onPress={()=>navigation.navigate('Tab')} style={{backgroundColor:"#438eff", justifyContent:'center', alignItems:'center', marginTop:200 , padding:20, borderRadius:15, margin:15}}>
                <View style={{flexDirection:'row',  justifyContent:'center', alignItems:'center'}}>
                    <Text style={{fontSize:16, fontWeight:'700', color:'white', paddingLeft:10}}>Get started!</Text>
                </View> 
            </TouchableOpacity>
        </View>
      
    )
}
function StartSmall ({ navigation}:RootStackParamList){
    return (
        <View>
            <View style={{justifyContent:'center', alignItems:'center', paddingTop:150}}>
                <Image source={HealthImage} style={{width: 270, height: 270}}></Image>
                <MainText text="Start small, go big" active={true}/>
                <MainText text="Start small and build your long term goals" active={false}></MainText>
            
            </View>
            <View style={{margin:15, paddingTop:200}}>
                <View style={{flexDirection:'row', justifyContent:"space-between", alignItems:'center'}}>
                    <SimpleButton text="Skip" fontSize={18} fontWeight="700"  onPress={()=>navigation.navigate('Tab')} color="black"></SimpleButton>
                    <SimpleButton onPress={()=>navigation.navigate('SplashScreen')}>
                        <MaterialCommunityIcons name="chevron-right-circle" size={50} color="#438eff"></MaterialCommunityIcons>
                    </SimpleButton>
                </View>
            </View>
        </View>
        
    )
}

function NewUserTodayHabitTab (){
    return(
        <View>
            <TabTitle text="Create New Habit"/>
        </View>
    )
}

function TodayHabit (){
    const [totalHabits, setTotalHabits] = useState([
        {title: 'Gym', type: 'gym', color: 'salmon', min: 20 },
        {title: 'Meditate', type: 'med', color: 'teal', min: 10 },
        {title: 'Run', type: 'gym', color: 'pink', min: 30 },
        {title: 'Run', type: 'gym', color: 'gold', min: 30 },
    ]);

    const loadData = async () => {
        const response = await fetch("",{});
        const temp = await response.json(); //
        setTotalHabits(temp);
    }

    useEffect(()=>{
        // loadData();
        console.log("Get Data");
    },[]);

    return (
        <View>
            <NewUserTodayHabitTab></NewUserTodayHabitTab>
            <View style={{flexDirection:'row'}}>
                <ChooseHabitButton  text="Medidate" active={true} onPress={()=>{console.log('clicked')}}></ChooseHabitButton>
            </View>

            <View style={{flexDirection:'row', marginEnd:25}}>
                <Text> Pending habits</Text>
                <Text>23th Jan, Thursday</Text>
            </View>
            {totalHabits.map(habit=>(<ShowHabits data={habit} />))}
            
            

            <View style={{flexDirection:'row', marginEnd:25}}>
                <Text> Completed Habits</Text>
            </View>
        </View>
    )
}
function AddHabitWithText ({addFb}){
    const [valText, setValText] = useState("");
    const add =(text) => {
        setValText(text)
        addFb(valText)
    }
    
    return(
        <View>
            <View>
                <MainText text="After, I" active={true} />
                <NewHabitInput  placeholder="Choose or type existing habit" value={valText} onChange={setValText} active></NewHabitInput>
            </View>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                <View style={{flexDirection:'row'}}>
                    <ChooseHabitButton  text="Medidate" active={true} onPress={setValText}></ChooseHabitButton>
                    <ChooseHabitButton  text="finish my gymnasium" active={true} onPress={setValText}></ChooseHabitButton>
                    <ChooseHabitButton  text="Medidate-1" active={true} onPress={setValText}></ChooseHabitButton>
                    <ChooseHabitButton  text="Medidate-2" active={true} onPress={setValText}></ChooseHabitButton>
                    <ChooseHabitButton  text="Medidate-3" active={true} onPress={setValText}></ChooseHabitButton>
                    <ChooseHabitButton  text="Medidate-4" active={true} onPress={setValText}></ChooseHabitButton>
                </View>
            </ScrollView>
        </View>
    )
}
function CreateNewHabit ({navigation}:RootStackParamList){


    const usersCollection = firestore().collection('Habits');
    const addFb = (value) => {
        firestore()
        .collection('Habits')
        .add({
            inputValue:value,
            age: 30,
        })
        .then(() => {
            console.log('User added!');
        });
    }
    const [color, setColor] = useState(true);   
    const [index1, setIndex1] = useState(0);
    const [pickcolor, setPickColor]=useState('');
    console.log("pickcolor",pickcolor);
    const handleClick=(index:any)=>{
        setIndex1(index);
        setColor((prevValue) => !prevValue);
    };
    const data = [{text : "All days"}, {text : "Mon"}, {text : "Tue"} , {text : "Wed"} , {text : "Thu"} , {text : "Fri"}, {text : "Sat"}, {text : "Sun"}] 
    const colorData= [{circleBackgroundColor:'#fad1d0'}, {circleBackgroundColor:'#c5e3ff'},{circleBackgroundColor:'#bce6e4'},{circleBackgroundColor:'#fbcb93'},]
    return (
        <ScrollView>
            <View style={{margin:15,}}>
                <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
                    <SimpleButton icon={true} onPress={()=>navigation.navigate('All habits')}></SimpleButton>
                    <TabTitle text="New Habit"/>
                    <SimpleButton text="Cancel" onPress={()=>navigation.navigate('Today')}></SimpleButton>
                </View>
                <View style={{marginTop:20}}>
                    <AddHabitWithText addFb = {addFb} ></AddHabitWithText>
                    <AddHabitWithText addFb = {addFb}></AddHabitWithText>
                </View>
                <MainText text="Click on days to remind" active={true}/>
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                    <View style={{flexDirection:'row'}}>
                        {data.map((el,index) => {
                            return (
                                <ChooseHabitButton text = {el.text} active={true} bgColor={color ? (index === index1 && "") : (index === index1 && "#6efd9d")} onPress = {() => handleClick(index)}/>
                            )
                        })}
                    </View>
                </ScrollView>
                <View>
                    <MainText  text="Select label color" active={true}/>
                    <View style={{flexDirection: 'row'}}>
                        {colorData.map((el, index) => {
                            return (
                                <Circle onPress={()=>pickcolor} circleBackgroundColor={el.circleBackgroundColor}/>
                            )
                        })}
                    </View>
                </View>
                <TouchableOpacity onPress={()=>addFb} style={{backgroundColor:"#61a0ff", justifyContent:'center', alignItems:'center', marginTop:40 , padding:20, borderRadius:15}}>
                    <View style={{flexDirection:'row',  justifyContent:'center', alignItems:'center'}}>
                        <MaterialIcons name="add-circle" size={20} color="white"></MaterialIcons>
                        <Text style={{fontSize:16, fontWeight:'700', color:'white', paddingLeft:10}}>Create new habit</Text>
                    </View> 
                    
                </TouchableOpacity>
            </View>     
        </ScrollView>
        
    )
}

function AllHabitsTab (){
    return (
        <View>
            <View>
                <Text>After I, </Text>
                <TextInput placeholder="Choose or type your existing habit"></TextInput>
            </View>
        </View>
    )
}
function AllHabits (){
    return (
        <View>
            <Text>AllHabits</Text>
        </View>
    )
}
function TabContainer (){
    return (
        <Tab.Navigator>
            <Tab.Screen name="Today" component={TodayHabit}></Tab.Screen>
            <Tab.Screen name="Create new" component={CreateNewHabit}></Tab.Screen>
            <Tab.Screen name="All habits" component={AllHabits}></Tab.Screen>
        </Tab.Navigator>
    )
}
function HabitStack (){
    return(
        <NavigationContainer>
            <Stack.Navigator initialRouteName="StartSmall" headerMode="none">
                <Stack.Screen name="StartSmall" component={StartSmall}></Stack.Screen>
                <Stack.Screen name="SplashScreen" component={SplashScreen}></Stack.Screen>
                <Stack.Screen name="Tab" component={TabContainer}></Stack.Screen>
            </Stack.Navigator>
        </NavigationContainer>
    )
}
export default HabitStack;