import React from 'react';
import { StyleSheet, Text, View, Image, Button, TouchableOpacity } from 'react-native';
import { NavigationContainer, TabActions } from '@react-navigation/native';
import { TextInput } from 'react-native-gesture-handler';
import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterilaCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"
type PrimaryButtonType={
    text: string,
    textColor?: string,
    bgColor?: any,
    onPress:  (text: string)=> any,
    onFocus? : any,
    borderStyle?: any,
    borderWidth?: number,
    active?: any,
    children?: any,
    marginTop?: number
}

export const ChooseHabitButton=({text, active, textColor, bgColor, borderStyle,borderWidth , children, marginTop, onFocus, onPress}:PrimaryButtonType)=>{
    return (
        <TouchableOpacity disabled={active ? false : true} style={[styles.button, {marginTop: marginTop || (active ? 15 : 0), 
            backgroundColor:bgColor || ( active ? "#e4e4e4" :"#ffffff"), borderStyle: borderStyle || 
            (active ? "dotted":"solid"), borderWidth: borderWidth || (active ? 2: 0)}]}
            onPress={()=>onPress(text)} onFocus={onFocus}>
                
            <Text style={[styles.buttonText, {color:textColor || (active ? "black" : "#e4e4e4")}]}>{text}</Text>
            {children}
        </TouchableOpacity>
    )
}

type SimpleButtonType = {
    icon?: any,
    onPress:  ()=> void,
    text?:string,
    color?:string,
    fontSize?: number,
    fontWeight?: any,
    children?: any
}

export const SimpleButton = ({icon, text, color, fontSize, fontWeight, children, onPress}:SimpleButtonType)=>{
    return(
        <TouchableOpacity onPress={onPress}>
            <View style={{flexDirection:'row'}}>
                {icon ? <Ionicons name="chevron-back-outline" color="#61a0ff" size={20}/> : <Text></Text>}
                <Text style={[styles.simplebuttontext, {color:color, fontSize:fontSize, fontWeight:fontWeight}]}>{text}</Text>
                {children}
            </View>
        </TouchableOpacity>
    )
}
type NewHabitInputType ={
    placeholder: string,
    value: string,
    keyboardType?: any,
    active: any,
    borderWidth?: number,
    width?: number,
    onChange: (text:string)=>any
    marginTop?: number,
    
}
export const NewHabitInput = ({placeholder, value, borderWidth, width, onChange, active,marginTop}:NewHabitInputType)=>{
    return (
        <TextInput value={value} style={[styles.textInput, {borderWidth: borderWidth||(active ? 3 : 6), 
            width:width || (active ? "100%" : 500 ), marginTop: marginTop || (active ? 10 : 0)}]} 
         placeholder={placeholder} onChangeText={text=>onChange(text)}></TextInput>
    )
}
type MainTextType = {
    marginTop?:number,
    fontsize?: number,
    fontWeight?: any,
    color?: string,
    active: boolean,
    text: string
}
export const MainText =({marginTop, fontsize, fontWeight, color, active, text}: MainTextType )=>{
    return(

        <Text style={[styles.mainText, {marginTop: marginTop || (active ? 20 : 0), fontSize: fontsize || (active ? 16 : 14), fontWeight: fontWeight || (active ? "400" : "bold"), color: color || (active ? "black" : "#9f9f9f")}]}>{text}</Text>
    )
}
type TabTitleType={
    text: string
}
export const TabTitle=({text}:TabTitleType)=>{
    return(
        <Text style={styles.titelText}>{text}</Text>
    )
}
type CircleType = {
    onPress: ()=> any,
    circleBackgroundColor: string
    
}
export const Circle = ( {circleBackgroundColor, onPress}:CircleType) => {
    return <TouchableOpacity onPress={onPress} style={[styles.circle, {backgroundColor:circleBackgroundColor}]} />;
};

const HealthImage = require('./../assets/Images/4220878.jpg')
type ShowHabitsType= {
    backgroundColor :any
}
export const ShowHabits = ({data}:{data:any})=>{
    return(
        <View style={{flexDirection: 'row', margin:10, backgroundColor:data.color, borderRadius:20, padding:10}}>
            <View>
                <MaterilaCommunityIcons name="emoticon-outline" size={65} color="#f3aaa9"></MaterilaCommunityIcons>
            </View>
            <View style={{marginLeft:25, justifyContent:'space-evenly'}}>
                <Text style={{fontSize: 18, fontWeight:'600'}}>{data.title}</Text>
                <Text style={{fontSize: 18, color:"#9a8874"}}>After, I brush my teeth</Text>
            </View>
        </View>
    )
}

const styles =StyleSheet.create({
    showHabit:{

    },
    
    button:{
        borderRadius: 50,
        borderColor: "#cbcbcb",
        alignItems: "center",
        backgroundColor: "#e4e4e4",
        marginEnd: 15,
        
    },
    titelText:{
        fontFamily: 'Gilroy',
        fontSize: 16,
        fontStyle: 'normal',
        fontWeight: '700',
        lineHeight: 18,
        textAlign: 'center',
        color:"black",
    },
    buttonText: {
        fontFamily: 'Gilroy',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: '400',
        lineHeight: 18,
        textAlign: 'left',
        color:"#7d7d7d",
        padding:8
    },
    textInput : {
        borderColor: "#e4e4e4",
        borderRadius: 10,
        textAlign:'left',
        paddingLeft: 20,
        paddingBottom: 15,
        paddingTop: 15,
        fontSize:15,
    },
    circle: {
        width: 40,
        height: 40,
        borderRadius: 100 / 2,
        marginEnd: 15,
        marginTop:15
    },
    mainText:{
        fontFamily: 'Gilroy',
        fontStyle: 'normal',
    },
    simplebuttontext:{
        color: "#61a0ff",
        fontSize:14,
        fontWeight:"400",
        fontStyle:"normal",
        fontFamily:"Gilroy"
    }
    
})