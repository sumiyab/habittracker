/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import {HabitButton} from './Components/Components';
import TabContainer from './src/Home';

AppRegistry.registerComponent(appName, () => TabContainer);
